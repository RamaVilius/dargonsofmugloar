import beat.da.knights.domain.Battle;
import beat.da.knights.domain.Dragon;
import beat.da.knights.domain.Knight;
import beat.da.knights.domain.Weather.WEATHER;
import beat.da.knights.strategy.NormalWeatherDragonPickerStrategy;

public class TestForFailing {


  public static void main(String[] main) {
    Battle btl = new Battle();
    Knight knight = new Knight();
    btl.setKnight(knight);
    btl.setWeather(WEATHER.NORMAL);

    knight.setAgility(7);
    knight.setArmor(0);
    knight.setAttack(6);
    knight.setEndurance(7);


    NormalWeatherDragonPickerStrategy strat = new NormalWeatherDragonPickerStrategy();
    strat.apply(new Dragon(), btl);
  }

}
