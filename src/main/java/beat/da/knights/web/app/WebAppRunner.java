package beat.da.knights.web.app;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WebAppRunner {

  private static final Logger LOG = LoggerFactory.getLogger(WebAppRunner.class);

  public static void main(String[] args) throws Exception {
      Server server = new Server(8089);

      ServletContextHandler contextHandler = new ServletContextHandler(server,"/knightSmasher");
      contextHandler.addServlet(MainServlet.class, MainServlet.URI_PATH);

      LOG.info("KNIGHT SMASHER web service started");
      LOG.info("");
      LOG.info("SMASH A SINGLE PUNNY KNIGHT by making GET requests to http://localhost:8089/knightSmasher/smash");
      LOG.info("");
      LOG.info("TO SMASH MORE THAN ONE PUNNY KNIGHT add \"battles\" parameter to uri request e.g:");
      LOG.info("    http://localhost:8089/knightSmasher/smash?battles=10");
      LOG.info("");
      LOG.info("Supported response types:");
      LOG.info("    application/json     : returns results as, you guessed it, json format");
      LOG.info("    text/plain (default) : returns results as csv file ");
      server.start();
  }

}
