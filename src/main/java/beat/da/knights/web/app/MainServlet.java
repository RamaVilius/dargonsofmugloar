package beat.da.knights.web.app;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import beat.da.knights.data.suppliers.BattleInitiator;
import beat.da.knights.data.suppliers.BattleSupplier;
import beat.da.knights.data.suppliers.CounteringDragonChooser;
import beat.da.knights.data.suppliers.WeatherConditionsApplier;
import beat.da.knights.domain.Battle;
import beat.da.knights.strategy.FoggyWeatherDragonPickerStrategy;
import beat.da.knights.strategy.LongDraughtWeatherDragonPickerStrategy;
import beat.da.knights.strategy.NormalWeatherDragonPickerStrategy;
import beat.da.knights.strategy.RainyWeatherDragonPickerStrategy;
import beat.da.knights.strategy.StormyWeatherDragonPickerStrategy;
import beat.da.knights.web.message.converters.BattleCsvResponseConverter;
import beat.da.knights.web.message.converters.BattleJsonResponseConverter;
import beat.da.knights.web.message.converters.MessageConverter;

public class MainServlet extends HttpServlet {

  private static final long serialVersionUID = -7551674119703126560L;

  private static final Logger LOG = LoggerFactory.getLogger(MainServlet.class);

  public static String URI_PATH = "/smash";

  private static final List<MessageConverter> responseConverters = Stream.of(new BattleJsonResponseConverter(),
                                                                                     new BattleCsvResponseConverter())
                                                                                 .collect(Collectors.toList());
  private static final MessageConverter defaultConverter = new BattleCsvResponseConverter();

  private BattleSupplier battleSupplier = new BattleSupplier();

  private WeatherConditionsApplier weatherApplier = new WeatherConditionsApplier();

  private BattleInitiator battleInitiator = new BattleInitiator();

  private CounteringDragonChooser dragonPicker = new CounteringDragonChooser( Stream.of( new NormalWeatherDragonPickerStrategy(),
                                                                                         new FoggyWeatherDragonPickerStrategy(),
                                                                                         new StormyWeatherDragonPickerStrategy(),
                                                                                         new LongDraughtWeatherDragonPickerStrategy(),
                                                                                         new RainyWeatherDragonPickerStrategy())
                                                                                    .collect(Collectors.toList()));


  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {

      int battleAmount = getRequestedBattles(req);

      LOG.debug("Recieved ordert to initiate {0} battle/s", battleAmount);

      MessageConverter msgConverter = responseConverters.stream().filter(o -> o.test(req)).findFirst()
          .orElse(defaultConverter);

      Battle[] finishedBattles = new Battle[battleAmount];
      int index = 0;
      while (battleAmount > 0){
        Battle battle = battleSupplier.get();
        weatherApplier.andThen(dragonPicker).andThen(battleInitiator).apply(battle);
        battleAmount--;
        finishedBattles[index++] = battle;
      }
      msgConverter.convert(resp, finishedBattles);
  }


  private int getRequestedBattles(HttpServletRequest req){
    int battleAmount = 0;
    try {
      battleAmount = Integer.parseInt(Optional.ofNullable(req.getParameter("battles")).orElse("1"));
    } catch (Exception e) {
      battleAmount = 1;
    }
    battleAmount = battleAmount < 1? 1: battleAmount;
    return battleAmount;
  }

}
