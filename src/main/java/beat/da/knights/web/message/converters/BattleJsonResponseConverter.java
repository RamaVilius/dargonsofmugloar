package beat.da.knights.web.message.converters;

import java.nio.charset.StandardCharsets;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.entity.ContentType;

import com.google.gson.Gson;

import beat.da.knights.domain.Battle;

public class BattleJsonResponseConverter implements MessageConverter{

  private static final Gson gson = new Gson();

  public void convert(HttpServletResponse response, Battle... battles) {
      response.setContentType(ContentType.APPLICATION_JSON.getMimeType());
      response.setCharacterEncoding(StandardCharsets.UTF_8.displayName());
      try {
        response.getWriter().append(gson.toJson(battles));
      } catch (Exception e) {
        e.printStackTrace();
      }
  }

  @Override
  public boolean test(HttpServletRequest request) {
    return ContentType.APPLICATION_JSON.getMimeType().equals(request.getHeader("Content-Type"));
  }

}
