package beat.da.knights.web.message.converters;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.entity.ContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import beat.da.knights.domain.Battle;
import beat.da.knights.domain.Weather.WEATHER;

public class BattleCsvResponseConverter implements MessageConverter{

  private static final String CSV_HEADER =
      "Battle;Weather;Knight agility;Knight armor;Knight attack;Knight endurance;Dragon wing strength;"
      + "Dragon claw sharpness;Dragon scale thickness;Dragon fire breath;Battle outcome; Battle message;\n";
  private static final Logger LOG = LoggerFactory.getLogger(BattleCsvResponseConverter.class);

  public void convert(HttpServletResponse response, Battle... battles) {
      response.setContentType(ContentType.TEXT_PLAIN.getMimeType());
      response.setCharacterEncoding(StandardCharsets.UTF_8.displayName());
      response.addHeader("Content-disposition", "attachment; filename=\"battleLog.csv\"");

    try {
      response.getWriter().append(CSV_HEADER);
      Stream.of(battles).filter(Objects::nonNull).forEach(battle -> {
        try {
          response.getWriter().append(getBattleDataAsCsvString(battle));
        } catch (Exception e) {
          LOG.error("Error while outputting results to csv file.",e);
        }
      });
    } catch (IOException e1) {
      LOG.error("Error while outputting results to csv file.",e1);
    }
  }

  @Override
  public boolean test(HttpServletRequest request) {
    return ContentType.TEXT_PLAIN.getMimeType().equals(request.getHeader("Content-Type"));
  }

  private String getBattleDataAsCsvString(Battle battle){
    if(WEATHER.STORM == battle.getWeather())
      return String.join("; ", String.valueOf(battle.getGameId()),
          String.valueOf(battle.getWeather()),
          String.valueOf(battle.getKnight().getAgility()),
          String.valueOf(battle.getKnight().getArmor()),
          String.valueOf(battle.getKnight().getAttack()),
          String.valueOf(battle.getKnight().getEndurance()),
          String.valueOf(""),
          String.valueOf(""),
          String.valueOf(""),
          String.valueOf(""),
          String.valueOf(battle.getResult().getStatus()),
          String.valueOf(battle.getResult().getMessage()), "\n");

      return String.join("; ", String.valueOf(battle.getGameId()),
          String.valueOf(battle.getWeather()),
          String.valueOf(battle.getKnight().getAgility()),
          String.valueOf(battle.getKnight().getArmor()),
          String.valueOf(battle.getKnight().getAttack()),
          String.valueOf(battle.getKnight().getEndurance()),
          String.valueOf(battle.getDragon().getWingStrength()),
          String.valueOf(battle.getDragon().getClawSharpness()),
          String.valueOf(battle.getDragon().getScaleThickness()),
          String.valueOf(battle.getDragon().getFireBreath()),
          String.valueOf(battle.getResult().getStatus()),
          String.valueOf(battle.getResult().getMessage()), "\n");
  }

}
