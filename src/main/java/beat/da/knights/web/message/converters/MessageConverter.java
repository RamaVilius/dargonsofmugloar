package beat.da.knights.web.message.converters;

import java.util.function.Predicate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beat.da.knights.domain.Battle;

public interface MessageConverter extends Predicate<HttpServletRequest> {

  public void convert(HttpServletResponse response, Battle ... battles);

}
