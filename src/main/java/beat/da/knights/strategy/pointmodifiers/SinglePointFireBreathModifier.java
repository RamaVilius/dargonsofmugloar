package beat.da.knights.strategy.pointmodifiers;

import java.util.function.BiFunction;

import beat.da.knights.domain.DistributablePointPool;
import beat.da.knights.domain.Dragon;

public class SinglePointFireBreathModifier implements BiFunction<Dragon, DistributablePointPool, Dragon>{

  public static final int MAX_THRESHOLD = 10;

  @Override
  public Dragon apply(Dragon dragon, DistributablePointPool points) {
    if( points.availablePoints() > 0 && dragon.fireBreath < MAX_THRESHOLD)
      dragon.fireBreath = dragon.fireBreath + points.retrieve(1);
    return dragon;
  }

}
