package beat.da.knights.strategy.pointmodifiers;

import beat.da.knights.domain.DistributablePointPool;
import beat.da.knights.domain.Dragon;
import beat.da.knights.domain.Knight;

public class WingStrengthReducer implements DragonAttributeModifier {

  public static final int REDUCABLE_POINTS = 1;

  @Override
  public Dragon apply(Dragon dragon, DistributablePointPool points) {

    if(dragon.getWingStrength() < REDUCABLE_POINTS)
      return dragon;

    points.addPoints(REDUCABLE_POINTS);
    dragon.setWingStrength(dragon.getWingStrength() - REDUCABLE_POINTS);
    return dragon;
  }

  @Override
  public boolean isApplicable(Knight knight, int maxAttr) {
    return maxAttr != knight.getAgility();
  }

}
