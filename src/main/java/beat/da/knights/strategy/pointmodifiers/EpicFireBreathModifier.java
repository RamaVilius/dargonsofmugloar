package beat.da.knights.strategy.pointmodifiers;

import beat.da.knights.domain.DistributablePointPool;
import beat.da.knights.domain.Dragon;
import beat.da.knights.domain.Knight;

public class EpicFireBreathModifier implements DragonAttributeModifier{

  public static final int ADDED_COUNTER_POINTS = 2;
  public static final int MAX_THRESHOLD = 10;

  //Do not exceed
  @Override
  public Dragon apply(Dragon dragon, DistributablePointPool points) {
    int addedPoints= pointsToAdd(dragon);
    if( points.availablePoints() >= addedPoints )
      dragon.fireBreath = dragon.fireBreath + points.retrieve(addedPoints);
    return dragon;
  }

  @Override
  public boolean isApplicable(Knight knight, int maxAttr) {
    return maxAttr == knight.getEndurance();
  }

  private int pointsToAdd (Dragon dragon) {
    int maxPointsToAdd = MAX_THRESHOLD - dragon.fireBreath;
    return  maxPointsToAdd < ADDED_COUNTER_POINTS ? maxPointsToAdd : ADDED_COUNTER_POINTS;
  }
}
