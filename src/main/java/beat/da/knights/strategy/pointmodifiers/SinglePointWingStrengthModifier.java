package beat.da.knights.strategy.pointmodifiers;

import java.util.function.BiFunction;

import beat.da.knights.domain.DistributablePointPool;
import beat.da.knights.domain.Dragon;

public class SinglePointWingStrengthModifier implements BiFunction<Dragon, DistributablePointPool, Dragon>{

  public static final int MAX_THRESHOLD = 10;

  @Override
  public Dragon apply(Dragon dragon, DistributablePointPool points) {
    if( points.availablePoints() > 0 && dragon.wingStrength < MAX_THRESHOLD)
      dragon.wingStrength = dragon.wingStrength + points.retrieve(1);
    return dragon;
  }

}
