package beat.da.knights.strategy.pointmodifiers;

import java.util.function.BiFunction;

import beat.da.knights.domain.DistributablePointPool;
import beat.da.knights.domain.Dragon;
import beat.da.knights.domain.Knight;

public interface DragonAttributeModifier extends BiFunction<Dragon, DistributablePointPool, Dragon>{

  /**
   * Determines whether modifier should be applied
   */
  boolean isApplicable(Knight knight, int maxAttr);
}
