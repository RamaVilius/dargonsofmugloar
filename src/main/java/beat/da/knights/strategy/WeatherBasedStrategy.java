package beat.da.knights.strategy;

import java.util.function.BiFunction;

import beat.da.knights.domain.Battle;
import beat.da.knights.domain.Dragon;
import beat.da.knights.domain.Weather;

/**
 * Marker for dragon picking strategies
 * that are applicable for a certain weather
 *
 */
public interface WeatherBasedStrategy extends BiFunction<Dragon, Battle, Battle>{

  /**
   * Checks if this strategy is applicable for a certain kind of weather
   */
  public boolean applicableForWeather (Weather.WEATHER weather);

}
