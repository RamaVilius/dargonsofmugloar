package beat.da.knights.strategy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import beat.da.knights.domain.Battle;
import beat.da.knights.domain.Dragon;
import beat.da.knights.domain.Weather.WEATHER;

/**
 * Picks dragon to counter knights that arrive with umbrellas
 */
public class RainyWeatherDragonPickerStrategy implements WeatherBasedStrategy{

  private static final Logger LOG = LoggerFactory.getLogger(RainyWeatherDragonPickerStrategy.class);

  /*
   * Checks if strategy is applicable for rainy weather
   */
  public boolean applicableForWeather(WEATHER weather) {
    return WEATHER.RAIN_WITH_FLOOD == weather;
  }

  /**
   * Picks dragon with additional claw sharpness to destroy umbrella upon
   * which the knight arrives.
   */
  @Override
  public Battle apply(Dragon counteringDragon, Battle battle) {
    LOG.debug("{0} strategy for picking dragon was applied for {1} battle.",
        this.getClass().getName(), battle.getGameId());

    counteringDragon.setClawSharpness(10);////to compensate for boat need max sharpness
    counteringDragon.setScaleThickness(10);// in case attack is HUGE
    counteringDragon.setWingStrength(0);//agility not important with lifepool
    counteringDragon.setFireBreath(0);//fire is useless

    battle.setDragon(counteringDragon);
    return battle;
  }
}
