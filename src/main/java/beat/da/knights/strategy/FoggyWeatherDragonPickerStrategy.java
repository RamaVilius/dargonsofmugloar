package beat.da.knights.strategy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import beat.da.knights.domain.Battle;
import beat.da.knights.domain.Dragon;
import beat.da.knights.domain.Weather.WEATHER;

/**
 * Picks dragon based on foggy weather strategy
 *
 */
public class FoggyWeatherDragonPickerStrategy implements WeatherBasedStrategy {

  private static final Logger LOG = LoggerFactory.getLogger(FoggyWeatherDragonPickerStrategy.class);

  /**
   * Distributes all dragon points equally as knight is useless in the fog.
   */
  public Battle apply(Dragon counteringDragon, Battle battle) {
    LOG.debug("{0} strategy for picking dragon was applied for {1} battle.",
              this.getClass().getName(), battle.getGameId());

    counteringDragon.setClawSharpness(battle.getKnight().getArmor());
    counteringDragon.setScaleThickness(battle.getKnight().getAttack());
    counteringDragon.setWingStrength(battle.getKnight().getAgility());
    counteringDragon.setFireBreath(battle.getKnight().getEndurance());

    battle.setDragon(counteringDragon);
    return battle;
  }

  public boolean applicableForWeather(WEATHER weather) {
    return WEATHER.FOGY == weather;
  }

}
