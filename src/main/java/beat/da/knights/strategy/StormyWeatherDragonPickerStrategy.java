package beat.da.knights.strategy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import beat.da.knights.domain.Battle;
import beat.da.knights.domain.Dragon;
import beat.da.knights.domain.Weather.WEATHER;

/**
 * Doesn't send dragon to battle if stormy weather
 *
 */
public class StormyWeatherDragonPickerStrategy implements WeatherBasedStrategy{

  private static final Logger LOG = LoggerFactory.getLogger(StormyWeatherDragonPickerStrategy.class);

  public boolean applicableForWeather(WEATHER weather) {
    return WEATHER.STORM == weather;
  }

  /**
   * The dragon doesn't have any suicidal intentions so he stays at home and takes a nap
   */
  @Override
  public Battle apply(Dragon counteringDragon, Battle battle) {
    LOG.debug("{0} strategy for picking dragon was applied for {1} battle.",
        this.getClass().getName(), battle.getGameId());

    battle.setDragon(null);
    return battle;
  }
}
