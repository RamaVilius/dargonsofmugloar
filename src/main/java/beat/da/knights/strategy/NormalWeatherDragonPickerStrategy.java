package beat.da.knights.strategy;

import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import beat.da.knights.domain.Battle;
import beat.da.knights.domain.DistributablePointPool;
import beat.da.knights.domain.Dragon;
import beat.da.knights.domain.Knight;
import beat.da.knights.domain.Weather.WEATHER;
import beat.da.knights.strategy.pointmodifiers.ClawSharpnessReducer;
import beat.da.knights.strategy.pointmodifiers.DragonAttributeModifier;
import beat.da.knights.strategy.pointmodifiers.EpicClawSharpnessModifier;
import beat.da.knights.strategy.pointmodifiers.EpicFireBreathModifier;
import beat.da.knights.strategy.pointmodifiers.EpicScaleThicknessModifier;
import beat.da.knights.strategy.pointmodifiers.EpicWingStrengthModifier;
import beat.da.knights.strategy.pointmodifiers.FireBreathReducer;
import beat.da.knights.strategy.pointmodifiers.ScaleThicknessReducer;
import beat.da.knights.strategy.pointmodifiers.SinglePointClawSharpnessModifier;
import beat.da.knights.strategy.pointmodifiers.SinglePointFireBreathModifier;
import beat.da.knights.strategy.pointmodifiers.SinglePointScaleThicknessModifier;
import beat.da.knights.strategy.pointmodifiers.SinglePointWingStrengthModifier;
import beat.da.knights.strategy.pointmodifiers.WingStrengthReducer;

public class NormalWeatherDragonPickerStrategy implements WeatherBasedStrategy {

  private static final Logger LOG = LoggerFactory.getLogger(NormalWeatherDragonPickerStrategy.class);

  private static final List<DragonAttributeModifier> minPassingAttrModifiers =
      Stream.of(new ScaleThicknessReducer(), new ClawSharpnessReducer(), new WingStrengthReducer(),
          new FireBreathReducer()).collect(Collectors.toList());

  private static final List<DragonAttributeModifier> epicDragonAttrModifiers = Stream
      .of(new EpicScaleThicknessModifier(), new EpicClawSharpnessModifier(),
          new EpicWingStrengthModifier(), new EpicFireBreathModifier())
      .collect(Collectors.toList());

  private static final List<BiFunction<Dragon, DistributablePointPool, Dragon>> pointRedistributors =
      Stream
          .of(new SinglePointScaleThicknessModifier(), new SinglePointClawSharpnessModifier(),
              new SinglePointWingStrengthModifier(), new SinglePointFireBreathModifier())
          .collect(Collectors.toList());



  public Battle apply(Dragon counteringDragon, Battle battle) {
    LOG.debug("{0} strategy for picking dragon was applied for {1} battle.",
        this.getClass().getName(), battle.getGameId());

    final Knight knight = battle.getKnight();
    final int epicAttributeVal = getEpicAttrVal(knight);
    final DistributablePointPool pointPool = new DistributablePointPool(0);

    BiFunction<Dragon, Knight, Dragon> dragonInitialStatsDistributor = (drg, kni) -> {
      drg.setClawSharpness(kni.getArmor());
      drg.setScaleThickness(kni.getAttack());
      drg.setWingStrength(kni.getAgility());
      drg.setFireBreath(kni.getEndurance());
      return drg;
    };

    dragonInitialStatsDistributor.apply(counteringDragon, battle.getKnight());

    minPassingAttrModifiers.stream().filter(o -> o.isApplicable(knight, epicAttributeVal))
        .forEach(modifier -> modifier.apply(counteringDragon, pointPool));

    if (!singleEpicAttr(knight, epicAttributeVal)) {
      minPassingAttrModifiers.stream().filter(o -> !o.isApplicable(knight, epicAttributeVal))
          .skip(1).forEach(modifier -> modifier.apply(counteringDragon, pointPool));
    }

    // retrieve points counter epic attributes
    epicDragonAttrModifiers.stream().filter(o -> o.isApplicable(knight, epicAttributeVal))
        .forEach(modifier -> modifier.apply(counteringDragon, pointPool));
    // redistribute remaining points
    if (pointPool.availablePoints() > 0) {
      pointRedistributors.forEach(mod -> mod.apply(counteringDragon, pointPool));
    }

    battle.setDragon(counteringDragon);
    return battle;
  }

  public boolean applicableForWeather(WEATHER weather) {
    return WEATHER.NORMAL == weather;
  }

  private int getEpicAttrVal(Knight knight) {
    return IntStream
        .of(knight.getArmor(), knight.getAttack(), knight.getAgility(), knight.getEndurance()).max()
        .getAsInt();
  }

  private boolean singleEpicAttr(Knight knight, final int epicValue) {
    return IntStream
        .of(knight.getArmor(), knight.getAttack(), knight.getAgility(), knight.getEndurance())
        .filter(val -> val == epicValue).count() == 1;
  }

}
