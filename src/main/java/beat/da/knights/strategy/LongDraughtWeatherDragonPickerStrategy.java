package beat.da.knights.strategy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import beat.da.knights.domain.Battle;
import beat.da.knights.domain.Dragon;
import beat.da.knights.domain.Weather.WEATHER;

/**
 * Provides the Zennest draggon of them all.
 */
public class LongDraughtWeatherDragonPickerStrategy implements WeatherBasedStrategy{

  private static final Logger LOG = LoggerFactory.getLogger(LongDraughtWeatherDragonPickerStrategy.class);

  public boolean applicableForWeather(WEATHER weather) {
    return WEATHER.LONG_DRY == weather;
  }

  /**
   * RELEASE THE ZEN DRAGON (all 20 points are distributed equally)!!!!
   */
  @Override
  public Battle apply(Dragon counteringDragon, Battle battle) {
    LOG.debug("{0} strategy for picking dragon was applied for {1} battle.",
        this.getClass().getName(), battle.getGameId());

    counteringDragon.setClawSharpness(5);
    counteringDragon.setScaleThickness(5);
    counteringDragon.setWingStrength(5);
    counteringDragon.setFireBreath(5);
    battle.setDragon(counteringDragon);
    return battle;
  }
}
