package beat.da.knights.data.suppliers;

import java.net.URI;
import java.util.function.Function;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import beat.da.knights.domain.Battle;
import beat.da.knights.domain.Weather.WEATHER;

/**
 * Retrieves and applies weather for battle
 */
public class WeatherConditionsApplier implements Function<Battle, Battle>{

  private static final Logger LOG = LoggerFactory.getLogger(WeatherConditionsApplier.class);

  private static final HttpClientBuilder HTTP_CLIENT_BUILDER = HttpClientBuilder.create();

  private static final XMLInputFactory XML_PARSER_FACTORY = XMLInputFactory.newInstance();

  public Battle apply(Battle battle) {

    HttpUriRequest request = new HttpGet(URI.create("http://www.dragonsofmugloar.com/weather/api/report/")
                                            .resolve(String.valueOf(battle.getGameId())));

    HttpClient client = HTTP_CLIENT_BUILDER.build();

    try {
      HttpResponse response  = client.execute(request);
      battle.setWeather(getWeatherFromCode(response));
    } catch (Exception e) {
      LOG.warn(String.format("Unable to retrieve weather conditions for battle: service called ( %s ). "
                              + "Default NORMAL_WEATHER will be used",request.getURI().toString()), e);
      battle.setWeather(WEATHER.NORMAL);
    }

    return battle;

  }

  /**
   * Parses retrieved xml and returns weather code
   */
  private WEATHER getWeatherFromCode (HttpResponse response) throws XMLStreamException {
    XMLEventReader eventReader = null;
    String code = "";
    try {
      eventReader = XML_PARSER_FACTORY.createXMLEventReader(response.getEntity().getContent());
      while(eventReader.hasNext()) {

        XMLEvent evt = eventReader.nextEvent();

        if(evt.getEventType() == XMLStreamReader.START_ELEMENT && "code".equals(evt.asStartElement().getName().getLocalPart())){
          code = eventReader.getElementText();
          break;
        }

      }
    } catch (Exception e) {
      LOG.error(String.format("Unable to parse weather code from from service response:/n  %s ",
                              response.getEntity().toString()), e);
    } finally {
      if(eventReader != null) eventReader.close();
    }
    LOG.debug("Retrieved weather code for battle: {0}",code);
    return WEATHER.resolveFromCode(code);
  }
}
