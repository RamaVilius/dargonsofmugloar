package beat.da.knights.data.suppliers;



import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.function.Supplier;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import beat.da.knights.domain.Battle;

/**
 * Supplies initial battle data
 */
public class BattleSupplier implements Supplier<Battle>{

  private static final Logger LOG = LoggerFactory.getLogger(BattleSupplier.class);

  private static final HttpClientBuilder CLIENT_BUILDER = HttpClientBuilder.create();
  private static final Gson JSON_MAPPER = new Gson();

  /**
   * Supply initial battle data
   */
  public Battle get() {

    HttpUriRequest request = new HttpGet("http://www.dragonsofmugloar.com/api/game");
    HttpClient client = CLIENT_BUILDER.build();
    try {
      HttpResponse response  = client.execute(request);
      return JSON_MAPPER.fromJson(new BufferedReader(new InputStreamReader(response.getEntity().getContent())), Battle.class);
    } catch (IOException e) {
      LOG.error(String.format(
          "Unable to retrieve data for battle: service called ( %s ).",
          request.getURI().toString()), e);
    }
    return null;
  }
}
