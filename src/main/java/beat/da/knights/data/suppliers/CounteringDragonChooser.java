package beat.da.knights.data.suppliers;

import java.util.Collection;
import java.util.function.Function;

import beat.da.knights.domain.Battle;
import beat.da.knights.domain.Dragon;
import beat.da.knights.strategy.WeatherBasedStrategy;

/**
 * Chooses countering dragon based on weather conditions and attacking knight attributes
 */
public class CounteringDragonChooser implements Function<Battle,Battle>{

  private Collection<WeatherBasedStrategy> weatherBasedPickers;

  public CounteringDragonChooser(Collection<WeatherBasedStrategy> availableStrategies){
    weatherBasedPickers = availableStrategies;
  }

  public Battle apply(final Battle battle) {
    weatherBasedPickers.stream()
              .filter(picker -> picker.applicableForWeather(battle.getWeather()))
              .findFirst()
              .ifPresent(picker -> picker.apply(new Dragon(), battle));
    return battle;
  }

}
