package beat.da.knights.data.suppliers;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.function.Function;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import beat.da.knights.domain.Battle;
import beat.da.knights.domain.BattleResult;

/**
 * Retrieves and applies weather for battle
 */
public class BattleInitiator implements Function<Battle, Battle> {

  private static final Logger LOG = LoggerFactory.getLogger(BattleInitiator.class);

  private static final HttpClientBuilder CLIENT_BUILDER = HttpClientBuilder.create();
  private static final Gson JSON_MAPPER = new Gson();

  public Battle apply(Battle battle) {
    HttpPut request = new HttpPut(URI.create("http://www.dragonsofmugloar.com/api/game/")
        .resolve(String.valueOf(battle.getGameId()) + "/").resolve("solution"));
    request.setEntity(new StringEntity(JSON_MAPPER.toJson(battle), ContentType.APPLICATION_JSON));

    HttpClient client = CLIENT_BUILDER.build();

    try {
      HttpResponse response = client.execute(request);
      BattleResult result = JSON_MAPPER.fromJson(
          new BufferedReader(new InputStreamReader(response.getEntity().getContent())),
          BattleResult.class);
      LOG.debug("Retrieved battle outcome for battle (id: {0}) -",battle.getGameId(), result.getStatus());
      battle.setResult(result);
    } catch (Exception e) {
      LOG.error(String.format(
          "Unable to retrieve solution calculation for battle: service called ( %s ).",
          request.getURI().toString()), e);
    }

    return battle;
  }
}
