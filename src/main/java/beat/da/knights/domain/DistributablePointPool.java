package beat.da.knights.domain;

public class DistributablePointPool {

    private int points = 0;

    public DistributablePointPool(int availablePoints){
      points = availablePoints;
    }

    public int retrieve(int amountToRetrieve) {
      if(points - amountToRetrieve < 0)
        return 0;

      points = points - amountToRetrieve;
      return amountToRetrieve;
    }

    public void addPoints(int pointsToAdd) {
      points = points + pointsToAdd;
    }

    public int availablePoints() {
      return points;
    }


}
