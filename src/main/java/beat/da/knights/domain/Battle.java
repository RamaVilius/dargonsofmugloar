package beat.da.knights.domain;

public class Battle {

  private long gameId = 0;

  private Knight knight;

  private Dragon dragon = new Dragon();

  private Weather.WEATHER weather;

  private BattleResult result;

  public long getGameId() {
    return gameId;
  }

  public void setGameId(long gameId) {
    this.gameId = gameId;
  }

  public Knight getKnight() {
    return knight;
  }

  public void setKnight(Knight knight) {
    this.knight = knight;
  }

  public Weather.WEATHER getWeather() {
    return weather;
  }

  public void setWeather(Weather.WEATHER weather) {
    this.weather = weather;
  }

  public BattleResult getResult() {
    return result;
  }

  public void setResult(BattleResult result) {
    this.result = result;
  }

  public Dragon getDragon() {
    return dragon;
  }

  public void setDragon(Dragon dragon) {
    this.dragon = dragon;
  }

}
