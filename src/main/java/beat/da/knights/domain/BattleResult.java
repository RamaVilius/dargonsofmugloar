package beat.da.knights.domain;

public class BattleResult {

  private String message = "";
  private String status = "";

  public String getMessage() {
    return message;
  }
  public void setMessage(String message) {
    this.message = message;
  }
  public String getStatus() {
    return status;
  }
  public void setStatus(String status) {
    this.status = status;
  }

}
