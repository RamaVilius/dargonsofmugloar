package beat.da.knights.domain;

import java.util.stream.Stream;

public class Weather {

  public static enum WEATHER {
    NORMAL("NMR"),
    FOGY("FUNDEFINEDG"),
    STORM("SRO"),
    RAIN_WITH_FLOOD("HVA"),
    LONG_DRY("T E"),
    WEATHER_ANOMALY("WEATHER_ANOMLY"); // <- unexpected weather

    private String code;

    WEATHER(String code) {
      this.code = code;
    }

    public String getCode(){
      return this.code;
    }

    public static WEATHER resolveFromCode (String code) {
      return Stream.of(WEATHER.values())
            .filter(val -> val.getCode().equals(code))
            .findFirst()
            .orElse(WEATHER_ANOMALY);
    }

  }

}
