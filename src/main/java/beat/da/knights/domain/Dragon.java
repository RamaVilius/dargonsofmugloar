package beat.da.knights.domain;

public class Dragon {

    public int scaleThickness = 0;
    public int clawSharpness = 0;
    public int wingStrength = 0;
    public int fireBreath = 0;

    public int getScaleThickness() {
      return scaleThickness;
    }

    public void setScaleThickness(int scaleThickness) {
      this.scaleThickness = scaleThickness;
    }

    public int getClawSharpness() {
      return clawSharpness;
    }

    public void setClawSharpness(int clawSharpness) {
      this.clawSharpness = clawSharpness;
    }

    public int getWingStrength() {
      return wingStrength;
    }

    public void setWingStrength(int wingStrength) {
      this.wingStrength = wingStrength;
    }

    public int getFireBreath() {
      return fireBreath;
    }

    public void setFireBreath(int fireBreath) {
      this.fireBreath = fireBreath;
    }

}
